\chapter{Resource allocation for nonlinearity-agnostic receiver}\label{chap:nl}

With the exponential increase in data traffic, satellite communication systems coupled with the next generation of cellular networks are becoming a key element.
We actually focus on uplink/return link satellite communications around Ka-band where the terrestrial antennas may correspond to relay points from terrestrial systems, the satellite acts as a relay to the final terrestrial gateway \cite{COGNITIF:evans2015,OPTI:lagunas2015}.
In this context, Chapter~\ref{chap:l} dealing with resource allocation and references therein, assumed that the satellite's \glsxtrfull{HPA} operates in a linear regime.
In Chapter~\ref{chap:capa}, a closed-form expression for the sum-rate has been derived when nonlinearity at the \acrshort{HPA} is considered.
Nevertheless, a smart resource allocation is not performed.
The main originality and contribution of this chapter is to take into account the nonlinear behavior of the satellite \acrshort{HPA} for deriving generic and practical resource allocation algorithms.
We consider that the data rate relies on the achievable data rate where the nonlinear interference is seen as an additional noise, called {\itshape nonlinearity-agnostic receiver}.  

When the \acrshort{HPA} operates as a nonlinear device, the data rate (obtained through the achievable data rate) depends on the nonlinear interference.
The case of linear interference has been widely studied in the literature dealing with wireless communications, e.g., \cite{OPTI:chiang2007,OPTI:stanczak2009}.
The case of nonlinear interference has been only pointed out in few papers \cite{OPTI:boyd2007,OPTI:stanczak2009}.

Actually, it is mentioned in \cite{OPTI:boyd2007} that the third-order intermodulation interference may be managed through \glsxtrfull{GP} for power optimization, but no simulations are performed.
In \cite{OPTI:stanczak2009}, only a class of  nonlinear interference is considered and does not fit with our case. 
This chapter is technically related to \cite{OPTI:chiang2007} since their tools remain valid in our case although this reference deals with linear interference.

The rest of the chapter is organized as follows. 
In Section~\ref{sec:chap5_problem_formulation}, the main problem of sum-rate maximization, for nonlinearity-agnostic receiver, is stated and is solved in Section~\ref{sec:chap5_maxsumrate}.
In Section~\ref{sec:chap5_maxminrate}, the fairness issue is addressed for nonlinearity-agnostic receiver, where the maximization of minimum per-user data rate problem is formulated and solved.
In Section~\ref{sec:chap5_numerical_results}, we take the liberty of dealing with the problem of sum-power minimization subject to a target data rate, for nonlinearity-agnostic receiver.
In Section~\ref{sec:chap5_conclusion}, concluding remarks are drawn.

\section{Problem formulation}\label{sec:chap5_problem_formulation}

The first problem to be solved, which is the main issue of the thesis, is that of maximizing the system sum-rate formulated in Problem~\ref{pb:main}.
We gave in Chapter~\ref{chap:capa} the data rate expression $\underline{R}_{b,m}$ for user belonging to beam $b$ using subband $m$ in the presence of nonlinear effects, according to a nonlinearity-agnostic receiver.
By putting \eqref{eq:capa_nl} in the main Problem~\ref{pb:main}, the resulting power optimization problem is
\begin{problem}\label{pb:nl_maxsumrate}
	\begin{align}
	\mathbf{P}^{\star} = & \arg \max_{\mathbf{P}} \sum_{b=1}^B \sum_{m=1}^M \log_2 \left(1+ Q_{b,m}\right) \nonumber \\
	\textnormal{with } & Q_{b,m} = \frac
	{
		\mathcal{P}_{b,m}^{(\textnormal{L})}
	}
	{ 
		\mathcal{P}_{b,m}^{(\textnormal{I})} + \mathcal{P}_{b,m}^{(\textnormal{NL})} + \mathcal{P}_\textnormal{W}
	} \label{eq:chap5_Q}\\
	\textnormal{s.t. } & \eqref{eq:contrainteFS} \textnormal{ and }  \eqref{eq:contrainteP}. \nonumber
	\end{align}
\end{problem}

Consequently, the powers of the users are all coupled through the expression of the data rate \eqref{eq:capa_nl} and the interference-temperature constraint \eqref{eq:contrainteFS}.
The optimization of the problem is challenging.
Indeed, the problem is non-convex due to the objective function which involves a ratio of the variable to be optimized.

\section{Solution for the maximization of the sum-rate}\label{sec:chap5_maxsumrate}

In this section, the Problem~\ref{pb:nl_maxsumrate} of maximization of the sum-rate is solved.
We have seen that the problem is non-convex.
Nevertheless, we proved in Chapter~\ref{chap:capa} that the terms $\mathcal{P}_{b,m}^{(\textnormal{I})}$ and $\mathcal{P}_{b,m}^{(\textnormal{NL})}$ involved in the data rate expression are posynomials.
The term $Q_{b,m}$ given by \eqref{eq:chap5_Q} is therefore a ratio of a monomial function over a posynomial function (since posynomials are closed under the addition).
In Chapter~\ref{chap:sp}, a method has been proposed to manage such a ratio. 

First, thanks to the monotonic growth of the logarithm function, Problem~\ref{pb:nl_maxsumrate} is equivalent to the following one.
\begin{problem}\label{pb:nl_maxsumrate_intermediaire1}
	\begin{align}
	\mathbf{P}^{\star} = & \arg \max_{\mathbf{P}} \prod_{b=1}^B \prod_{m=1}^M 1+ \frac
	{
		\mathcal{P}_{b,m}^{(\textnormal{L})}
	}
	{ 
		\mathcal{P}_{b,m}^{(\textnormal{I})} + \mathcal{P}_{b,m}^{(\textnormal{NL})} + \mathcal{P}_\textnormal{W}
	}  \\
	\textnormal{s.t. } & \eqref{eq:contrainteFS} \textnormal{ and }  \eqref{eq:contrainteP}. \nonumber
	\end{align}
\end{problem}

\begin{remark}
	Note that in high \acrshort{SINR} regime, we can use the approximation of the logarithm $\log(1+x) \approx \log(x)$.
	In that case, the resulting problem maximizes a ratio of monomial function over posynomial function (alternatively, minimizes a ratio of posynomial over monomial), leading to a valid posynomial and finally a problem in \acrshort{GP} form.
\end{remark}

Then, Problem ~\ref{pb:nl_maxsumrate_intermediaire1} is rewritten using the inverse of the objective function, as follows,
\begin{problem}\label{pb:nl_maxsumrate_rewritten}
	\begin{align}
	\mathbf{P}^{\star} = & \arg \min_{\mathbf{P}} \prod_{b=1}^B \prod_{m=1}^M
	\frac
	{ 
		\mathcal{P}_{b,m}^{(\textnormal{I})} + \mathcal{P}_{b,m}^{(\textnormal{NL})} + \mathcal{P}_\textnormal{W}
	}
	{
		\mathcal{P}_{b,m}^{(\textnormal{L})} + \mathcal{P}_{b,m}^{(\textnormal{I})} + \mathcal{P}_{b,m}^{(\textnormal{NL})} + \mathcal{P}_\textnormal{W}
	} \label{eq:chap5_objective_function}\\
	\textnormal{s.t. } & \eqref{eq:contrainteFS} \textnormal{ and }  \eqref{eq:contrainteP}. \nonumber
	\end{align}
\end{problem}

The difficulty is still located in the objective function, where we have a ratio of posynomial functions, so the problem is non-convex and boils down to \glsxtrfull{CGP}.
In Chapter~\ref{chap:sp}, we presented a standard way for reaching a stationary point of \acrshort{CGP} problem.
First, we use the so-called \glsxtrfull{SCA} method for non-convex problem, presented in Chapter~\ref{chap:sp}.
In order to apply \acrshort{SCA} procedure, our objective is now to find a tight upper-bound for \eqref{eq:chap5_objective_function} which is either directly convex or \acrshort{GP}.
Secondly, we propose to replace the denominator with a monomial satisfying the \acrshort{SCA} conditions at a given point.
A straightforward and easy way was shown in Section~\ref{sec:chap2_cgp} to build this monomial which satisfies \acrshort{SCA} conditions.
Finally, this leads to a \acrshort{GP} problem since a ratio of posynomial and monomial is a posynomial.
Since the monomial approximation satisfies the \acrshort{SCA} conditions, the \acrshort{SCA} procedure converges toward a stationary point.
In \cite{OPTI:chiang2007}, the authors use the same approach where, in their case, the interference is linear.

We denote the posynomial of the denominator by
\begin{equation}\label{eq:nl_maxmin_posynomial_denominator}
D_{b,m}(\mathbf{P}) = \mathcal{P}_{b,m}^{(\textnormal{L})} + \mathcal{P}_{b,m}^{(\textnormal{I})} + \mathcal{P}_{b,m}^{(\textnormal{NL})} + \mathcal{P}_\textnormal{W}.
\end{equation}

The monomial approximation at the point $\mathbf{P}_i$ is denoted by $\widetilde{D}_{b,m}^{(i)}$.
This approximation is built in such a way that it satisfies the \acrshort{SCA} conditions, thanks to the method explained in Section~\ref{sec:chap2_cgp},
\begingroup
\allowdisplaybreaks
\begin{align}\label{eq:nl_maxsumrate_monomial_denominator}
& \widetilde{D}_{b,m}^{(i)}(\mathbf{P}) =  \left( \frac{\gamma_1^2 G_{m}^{(b)} P_{b,m}}{\lambda_{b,m}^{(1)}} \right)^{\lambda_{b,m}^{(1)}} \nonumber \\
& \times \prod_{\substack{b'=1 \\ b' \ne b}}^{B} \left( \frac{\gamma_1^2 G_{m}^{(b',b)} P_{b',m}}{\lambda_{b,m}^{(2)}(b')} \right)^{\lambda_{b,m}^{(2)}(b')}\nonumber \\
& \times \prod_{b_1,b_2,b_3}^{B} \prod_{m',m''=1}^{M} \left( \frac{4 \gamma_3^2 \alpha^{(1)}_{0} G_{m}^{(b_1,b)} G_{m'}^{(b_2,b)} G_{m''}^{(b_3,b)} P_{b_1,m} P_{b_2,m'} P_{b_3,m''}}{\lambda_{b,m}^{(3)}(b_1,b_2,b_3,m',m'')} \right)^{\lambda_{b,m}^{(3)}(b_1,b_2,b_3,m',m'')} \nonumber \\
& \times \prod_{b_1,b_2,b_3=1}^{B} \prod_{m',m''=1}^{M} \left( \frac{4 \tilde{\delta}_{m,M} \gamma_3^2 \alpha^{(1)}_{1} G_{m+1}^{(b_1,b)} G_{m'}^{(b_2,b)} G_{m''}^{(b_3,b)} P_{b_1,m+1} P_{b_2,m'} P_{b_3,m''}}{\lambda_{b,m}^{(4)}(b_1,b_2,b_3,m',m'')} \right)^{\lambda_{b,m}^{(4)}(b_1,b_2,b_3,m',m'')} \nonumber \\ 
& \times \prod_{b_1,b_2,b_3=1}^{B} \prod_{m',m''=1}^{M} \left( \frac{4 \tilde{\delta}_{m,1} \gamma_3^2 \alpha^{(1)}_{1} G_{m-1}^{(b_1,b)} G_{m'}^{(b_2,b)} G_{m''}^{(b_3,b)} P_{b_1,m-1} P_{b_2,m'} P_{b_3,m''}}{\lambda_{b,m}^{(5)}(b_1,b_2,b_3,m',m'')} \right)^{\lambda_{b,m}^{(5)}(b_1,b_2,b_3,m',m'')} \nonumber \\
& \times \prod_{b_1,b_2,b_3}^{B} \prod_{\substack{m_1,m_2,m_3 =1 \\ m=m_1+m_2-m_3}}^{M} \left( \frac{2 \gamma_3^2 \alpha^{(2)}_{0} G_{m_1}^{(b_1,b)} G_{m_2}^{(b_2,b)} G_{m_3}^{(b_3,b)} P_{b_1,m_1} P_{b_2,m_2} P_{b_3,m_3}}{\lambda_{b,m}^{(6)}(b_1,b_2,b_3,m_1,m_2,m_3)} \right)^{\lambda_{b,m}^{(6)}(b_1,b_2,b_3,m_1,m_2,m_3)} \nonumber \\
& \!\!\!\! \times \!\! \prod_{b_1,b_2,b_3=1}^{B} \prod_{\substack{m_1,m_2,m_3 =1 \\ m=m_1+m_2-m_3 \pm 1}}^{M} \!\! \left( \frac{ 2 \gamma_3^2 \alpha^{(2)}_{1} G_{m_1}^{(b_1,b)} G_{m_2}^{(b_2,b)} G_{m_3}^{(b_3,b)} P_{b_1,m_1} P_{b_2,m_2} P_{b_3,m_3}}{\lambda_{b,m}^{(7)}(b_1,b_2,b_3,m_1,m_2,m_3)} \right)^{\lambda_{b,m}^{(7)}(b_1,b_2,b_3,m_1,m_2,m_3)} \nonumber \\
& \times \left( \frac{\mathcal{P}_\textnormal{W}}{\lambda_{b,m}^{(7)}} \right)^{\lambda_{b,m}^{(7)}},
\end{align}
where
\begin{align}
& \lambda_{b,m}^{(1)} = \frac{\gamma_1^2 G_{m}^{(b)} P_{b,m}^{(i)}}{D_{b,m}(\mathbf{P}_i)}, \\
& \lambda_{b,m}^{(2)}(b') = \frac{\gamma_1^2 G_{m}^{(b',b)} P_{b',m}^{(i)}}{D_{b,m}(\mathbf{P}_i)}, \\
& \lambda_{b,m}^{(3)}(b_1,b_2,b_3,m',m'') = \frac{4 \gamma_3^2 \alpha^{(1)}_{0} G_{m}^{(b_1,b)} G_{m'}^{(b_2,b)} G_{m''}^{(b_3,b)} P_{b_1,m}^{(i)} P_{b_2,m'}^{(i)} P_{b_3,m''}^{(i)}}{D_{b,m}(\mathbf{P}_i)}, \\
& \lambda_{b,m}^{(4)}(b_1,b_2,b_3,m',m'') = \frac{4 \tilde{\delta}_{m,M} \gamma_3^2 \alpha^{(1)}_{1} G_{m+1}^{(b_1,b)} G_{m'}^{(b_2,b)} G_{m''}^{(b_3,b)} P_{b_1,m+1}^{(i)} P_{b_2,m'}^{(i)} P_{b_3,m''}^{(i)}}{D_{b,m}(\mathbf{P}_i)}, \\
& \lambda_{b,m}^{(5)}(b_1,b_2,b_3,m',m'') = \frac{4 \tilde{\delta}_{m,1} \gamma_3^2 \alpha^{(1)}_{1} G_{m-1}^{(b_1,b)} G_{m'}^{(b_2,b)} G_{m''}^{(b_3,b)} P_{b_1,m-1}^{(i)} P_{b_2,m'}^{(i)} P_{b_3,m''}^{(i)}}{D_{b,m}(\mathbf{P}_i)}, \\
& \lambda_{b,m}^{(6)}(b_1,b_2,b_3,m_1,m_2,m_3) = \frac{2 \gamma_3^2 \alpha^{(2)}_{0} G_{m_1}^{(b_1,b)} G_{m_2}^{(b_2,b)} G_{m_3}^{(b_3,b)} P_{b_1,m_1}^{(i)} P_{b_2,m_2}^{(i)} P_{b_3,m_3}^{(i)}}{D_{b,m}(\mathbf{P}_i)}, \\
& \lambda_{b,m}^{(7)}(b_1,b_2,b_3,m_1,m_2,m_3) = \frac{2 \gamma_3^2 \alpha^{(2)}_{1} G_{m_1}^{(b_1,b)} G_{m_2}^{(b_2,b)} G_{m_3}^{(b_3,b)} P_{b_1,m_1}^{(i)} P_{b_2,m_2}^{(i)} P_{b_3,m_3}^{(i)}}{D_{b,m}(\mathbf{P}_i)}, \\
& \lambda_{b,m}^{(8)} = \frac{\mathcal{P}_\textnormal{W}}{D_{b,m}(\mathbf{P}_i)}.
\end{align}
\endgroup

\begin{lemma}
	The term $\widetilde{D}_{b,m}^{(i)}$ is a monomial approximation of $D_{b,m}$ at the point $\mathbf{P}_i$ which satisfies the \acrshort{SCA} conditions, shown in Section~\ref{sec:chap2_cgp}.
\end{lemma}

By replacing $D_{b,m}$ with $\widetilde{D}_{b,m}^{(i)}$ in Problem~\ref{pb:nl_maxsumrate_rewritten}, the resulting optimization problem is
\begin{problem}\label{pb:nl_maxsumrate_gp_sca}
	\begin{align}
	\mathbf{P}^{\star}_{i} = & \arg \min_{\mathbf{P}} \prod_{b=1}^B \prod_{m=1}^M \left(\widetilde{D}_{b,m}^{(i-1)}(\mathbf{P})\right)^{-1} \left( \mathcal{P}_{b,m}^{(\textnormal{I})} + \mathcal{P}_{b,m}^{(\textnormal{NL})} + \mathcal{P}_\textnormal{W} \right) \nonumber \\
	\textnormal{s.t. } & \eqref{eq:contrainteFS} \textnormal{ and }  \eqref{eq:contrainteP}. \nonumber
	\end{align}
\end{problem}

\begin{result}
	Problem~\ref{pb:nl_maxsumrate_gp_sca} is \acrshort{GP} since it minimizes a posynomial function over a set in \acrshort{GP} form and can be efficiently solved by numerical algorithms.
\end{result}

\begin{proof}
	The objective function is a posynomial function since the inverse of a monomial is still a monomial, and the posynomials are close under the addition and multiplication. 
\end{proof}

Since the monomial approximation satisfies the \acrshort{SCA} conditions, the \acrshort{SCA} procedure depicted in Algorithm~\ref{algo:nl_maxsumcapa} converges towards a stationary point.
The obtained solution is sub-optimal.
As starting feasible point, we can use the solution obtained by Algorithm~\ref{algo:l_maxsumcapa_posy}.

\begin{algorithm}[htb]
	\caption{\acrshort{SCA} based procedure for solving Problem~\ref{pb:nl_maxsumrate}}
	\begin{algorithmic}[1]\label{algo:nl_maxsumcapa}
		\STATE Set $\epsilon > 0$, $E = \epsilon + 1$, $i = 0$
		\STATE Find $\mathbf{P}_0$ a feasible solution of Problem~\ref{pb:nl_maxsumrate} 
		\STATE Compute the sum-rate $R_0$ using \eqref{eq:capa_nl}
		\WHILE {$E > \epsilon$}
		\STATE $i=i+1$
		\STATE Compute the monomial approximation $\widetilde{D}_{b,m}^{(i-1)}(\mathbf{P})$ around the point $\mathbf{P}_{i-1}$, using \eqref{eq:nl_maxsumrate_monomial_denominator}
		\STATE Find $\mathbf{P}^{\star}_i$ the optimal solution of Problem~\ref{pb:nl_maxsumrate_gp_sca}
		\STATE Compute the sum-rate $R_{i}$ and $E = \left|R_{i}-R_{i-1}\right|$
		\ENDWHILE
		\RETURN $\mathbf{P}^\star=\mathbf{P}_{i}$
	\end{algorithmic} 
\end{algorithm}

In this section, we have proposed an optimization algorithm for the problem of system sum-rate maximization, in presence of nonlinear effects and when a nonlinearity-agnostic receiver is considered.
However, the notion of fairness is not present in this problem.
Indeed, the best users will be assigned the best data rates, while the worst users will have very low data rates.

\section{Maximization of the minimum per-user rate}\label{sec:chap5_maxminrate}

In this section, we address the fairness issue between users in presence of nonlinear effects and for the nonlinearity-agnostic receiver.
Here we consider only the {\itshape max-min fairness}.
The problem of maximization of the minimum per-user rate is formulated as following
\begin{problem}\label{pb:nl_maxmin}
	\begin{align}
	\mathbf{P}^{\star} = & \arg \max_{\mathbf{P}} \min_{b,m} \log_2 \left(1+ Q_{b,m}\right) \nonumber \\
	\textnormal{with } & Q_{b,m} = \frac
	{
		\mathcal{P}_{b,m}^{(\textnormal{L})}
	}
	{ 
		\mathcal{P}_{b,m}^{(\textnormal{I})} + \mathcal{P}_{b,m}^{(\textnormal{NL})} + \mathcal{P}_\textnormal{W}
	} \nonumber \\
	\textnormal{s.t. } & \eqref{eq:contrainteFS} \textnormal{ and }  \eqref{eq:contrainteP}. \nonumber
	\end{align}
\end{problem}

Thanks to the monotonic growth of the logarithm function, Problem~\ref{pb:nl_maxmin} is equivalent to the following one.
\begin{problem}\label{pb:nl_maxmin_sinr}
	\begin{align}
	\mathbf{P}^{\star} = & \arg \max_{\mathbf{P}} \min_{b,m} Q_{b,m} \nonumber \\
	\textnormal{s.t. } & \eqref{eq:contrainteFS} \textnormal{ and }  \eqref{eq:contrainteP}. \nonumber
	\end{align}
\end{problem}

The difficulty is located in the objective function, which is not concave nor a posynomial function because of the minimum operator and the ratio of a monomial function over a posynomial function $Q_{b,m}$.
First, we introduce a new variable $t \in \mathbb{R}^{+*}$ in order to remove the minimum operator.
Problem~\ref{pb:nl_maxmin_sinr} becomes
\begin{problem}\label{pb:nl_maxmin_max_t}
	\begin{align}
	\left(\mathbf{P}^{\star},  t^{\star}\right) = & \arg \max_{\mathbf{P},t} t \nonumber \\
	\textnormal{s.t. } & \eqref{eq:contrainteFS}, \eqref{eq:contrainteP}, \nonumber \\
	& Q_{b,m} \ge t, \quad \forall b,m. \label{eq:maxmin_ratio_posynomial}
	\end{align}
\end{problem}

\begin{remark}
	We notice that $\max t$ is equivalent to $\min t^{-1}$
\end{remark}

The objective function of Problem~\ref{pb:nl_maxmin_max_t} is now a valid monomial function.
Moreover, the constraint \eqref{eq:maxmin_ratio_posynomial} can be rewritten as
\begin{equation}\label{eq:nl_maxmin_posynomial}
{\mathcal{P}_{b,m}^{(\textnormal{L})}}^{-1}	t \left( \mathcal{P}_{b,m}^{(\textnormal{I})} + \mathcal{P}_{b,m}^{(\textnormal{NL})} + \mathcal{P}_\textnormal{W} \right) \le 1, \quad \forall b,m
\end{equation}

\begin{lemma}
	The constraint \eqref{eq:nl_maxmin_posynomial} has the form of a posynomial less than or equal to one, leading to a valid constraint for \acrshort{GP}.
\end{lemma}

\begin{proof}
	The term ${\mathcal{P}_{b,m}^{(\textnormal{L})}}^{-1}$ is a monomial function. By reminding that posynomials are close to addition and multiplication, the \glsxtrfull{LHS} is a posynomial function.
\end{proof}

The resulting optimization problem writes as
\begin{problem}\label{pb:nl_maxmin_gp}
	\begin{align}
	\left(\mathbf{P}^{\star},  t^{\star}\right) = & \arg \min_{\mathbf{P},t} t^{-1} \nonumber \\
	\textnormal{s.t. } & \eqref{eq:contrainteFS} \textnormal{, }  \eqref{eq:contrainteP} \textnormal{ and } \eqref{eq:nl_maxmin_posynomial}.\nonumber
	\end{align}
\end{problem}

\begin{result}
	Problem~\ref{pb:nl_maxmin_gp} is \acrshort{GP} since it minimizes of monomial (which is a posynomial term) over a set in \acrshort{GP} form.
\end{result}

Finally, we obtained a \acrshort{GP} problem after a clever rewriting of the problem when the user data rate is evaluated by \eqref{eq:capa_nl} for a nonlinearity-agnostic receiver.
We recall that \acrshort{GP} problems are easily solved by numerical solvers.

\section{Minimization of the sum-power}\label{sec:chap5_minsumpower}

In this section, we also cover the sum-power minimization problem, subject to a target data rate constraint.
Indeed, this problem can be easily solved when the \acrshort{SINR} is written as a posynomial ratio, which is our case for a nonlinearity-agnostic receiver.
The problem of minimization of the sum-power is formulated as follows
\begin{problem}\label{pb:nl_minsumpower}
	\begin{align}
	\mathbf{P}^{\star} = & \arg \min_{\mathbf{P}} \sum_{b=1}^{B} \sum_{m=1}^{M} P_{b,m} \nonumber \\
	\textnormal{s.t. } & \eqref{eq:contrainteFS} \textnormal{ and }  \eqref{eq:contrainteP}, \nonumber \\
	& \log_2 \left(1+ Q_{b,m}\right) \ge {R}_{b,m}^t, \quad \forall b,m, \label{eq:nl_minsumpower_contrainteR}\\
	\textnormal{with } & Q_{b,m} = \frac
	{
		\mathcal{P}_{b,m}^{(\textnormal{L})}
	}
	{ 
		\mathcal{P}_{b,m}^{(\textnormal{I})} + \mathcal{P}_{b,m}^{(\textnormal{NL})} + \mathcal{P}_\textnormal{W}
	} \nonumber \label{eq:minsumpower_ratio_posynomial}.
	\end{align}
\end{problem}
where ${R}_{b,m}^t$ is the target data rate for user belonging to beam $b$ using subband $m$.
Note that the problem may be infeasible if ${R}_{b,m}^t$ is too large.
In the case of a linear interference, the feasibility conditions are given in \cite{COM:tse2005}.

The formulation of Problem~\ref{pb:nl_minsumpower} is neither concave nor of the \acrshort{GP} form.
It is therefore impossible to use tools to solve it analytically or numerically with acceptable complexity, i.e., in polynomial time.
The main contribution of this section is to rewrite this problem in a standard form, i.e., concave or \acrshort{GP}, in order to be able to apply the standard tools of convex or \acrshort{GP} optimization.

The difficulty is located in the constraint \eqref{eq:nl_minsumpower_contrainteR}, where we have ratios of posynomial functions.
However, this constraint can be rewritten as

\begin{equation}
\left(  2^{{R}_{b,m}^t} - 1 \right) {\mathcal{P}_{b,m}^{(\textnormal{L})}}^{-1} \left( \mathcal{P}_{b,m}^{(\textnormal{I})} + \mathcal{P}_{b,m}^{(\textnormal{NL})} + \mathcal{P}_\textnormal{W} \right) \le 1, \quad \forall b,m. \label{eq:nl_minsumpower_posynomial}
\end{equation}

\begin{lemma}
	The constraint \eqref{eq:nl_minsumpower_posynomial} has the form of a posynomial less than or equal to one, leading to a valid constraint for \acrshort{GP}.
\end{lemma}

\begin{proof}
	The term $2^{{R}_{b,m}^t} - 1$ is a positive scalar, ${\mathcal{P}_{b,m}^{(\textnormal{L})}}^{-1}$ is a monomial function. Thanks to the addition rule of posynomial, the rest of the \acrshort{LHS} is a posynomial function. Finally, thanks to multiplication rule between a monomial and a posynomial function, the \acrshort{LHS} is a posynomial function.
\end{proof}

The resulting optimization problem writes as
\begin{problem}\label{pb:nl_minsumpower_gp}
	\begin{align}
	\mathbf{P}^{\star} = & \arg \min_{\mathbf{P}} \sum_{b=1}^{B} \sum_{m=1}^{M} P_{b,m} \nonumber \\
	\textnormal{s.t. } & \eqref{eq:contrainteFS} \textnormal{, }  \eqref{eq:contrainteP} \textnormal{ and } \eqref{eq:nl_minsumpower_posynomial}. \nonumber \\
	\end{align}
\end{problem}

\begin{result}
	Problem~\ref{pb:nl_minsumpower_gp} is \acrshort{GP} since it minimizes of posynomial over a set in \acrshort{GP} form.
\end{result}

Finally, we obtain a \acrshort{GP} problem after an intelligent rewriting of the problem when the user data rate is evaluated by \eqref{eq:capa_nl} for a nonlinearity-agnostic receiver.
We recall that \acrshort{GP} problems are easily solved by numerical solvers.


\section{Numerical results}\label{sec:chap5_numerical_results}

We consider a multibeam satellite system operating in the Ka-band for the uplink ($27.5$-$29.5$ GHz).
The satellite is composed of $B=2$ beams and there are $M=6$ users per beam.
The subband assignment has been already performed. 
The \acrshort{HPA} distortion coefficients $\gamma_1$ and $\gamma_3$ are $1$ and $0.05$ respectively.
In addition, we add a variable gain pre-amplifier just before the \acrshort{HPA}.
This device allows to set the \acrshort{HPA} regime by changing the channel gains (and so input powers) uniformly for incoming signal of the same antenna.
For simplicity, we assume that the gains of the pre-amplifiers are identical for all \acrshort{HPA}s. 
The shaping filter is a \glsxtrfull{SRRC} filter with roll-off $0.25$ for all users.
The maximum power is  $P_{\max} = 50$W ($47$dBm).
The primary \glsxtrfull{FS} subband contains $S=2$ \glsxtrfull{FSS} subbands.
The channel gains $\{G_{m}^{(b',b)}\}_{b,m,b'}$ and $\{F_{b,m}^{(\ell)}\}_{b,m,\ell}$ are computed according to \cite{OPTI:lagunas2015,SATCOM:caini1992,SATCOM:maleki2015,SATCOM:bousquet2011}.
Notice that the channel gain values within a same beam $\{G_m^{(b)}\}_{m}$ are close to each others. 
The maximum interference-temperature level is fixed to $I_{th}^{(\ell)}(m')=1$pW ($-90$dBm) for any $\ell,m'$.
Unless otherwise stated, we consider $1$ \acrshort{FS} per square kilometer.
We use the CVX toolbox to solve \acrshort{GP} problems \cite{OPTI:cvx}.

\subsection{Sum-rate analysis}

We denote the power allocations related to the maximization of the sum-rate by
\begin{itemize}
	\item $P^{\textnormal{naive1}}$, the power allocation where the users use the same transmit power $P$, which is then optimized for maximizing the system sum-rate where the nonlinear interference is taken into account,
	
	\item $P^{\textnormal{naive2}}$, the power allocation where the received power at the satellite level is the same, so $G_m^{(b)} P_{b,m} = G_{m'}^{(b')} P_{b',m'}$, which is then optimized for maximizing the system sum-rate where the nonlinear interference is taken into account,
	
	\item $P^{\textnormal{sota1}}$, the power allocation given by \cite{OPTI:lagunas2015} and detailed in Section~\ref{sec:chap3_solution_IBIF} where the (linear and nonlinear) interference is not taking into account,
	
	\item $P^\prime$, the power allocation given by Algorithm~\ref{algo:l_ibif_simplified} related to Problem~\ref{pb:l_ibif}, where there is no interference,
	
	\item $P^{\textnormal{li}}$, the power allocation given by Algorithm~\ref{algo:l_maxsumcapa_posy} related to Problem~\ref{pb:l_sbd_ratio_posynomial}, where only linear interference occurs, i.e. $\mathcal{P}_{b,m}^{(\textnormal{NL})} = 0$,
	
	\item $P^\star$, the power allocation given by Algorithm~\ref{algo:nl_maxsumcapa} which is the \acrshort{SCA} procedure for Problem~\ref{pb:nl_maxsumrate}.
\end{itemize}
In solid line, the sum-rate is evaluated with \eqref{eq:capa_nl} for nonlinearity-agnostic receiver.
In addition, we display in dotted line the sum-rate for the linear inter-beam interference case, i.e., when we enforce no nonlinear interference in the expression of the data rate \eqref{eq:capa_nl} and finally obtain the data rate \eqref{eq:sbd}.

In Fig.~\ref{fig:chap5_sumrate_vs_gain}, we plot the sum-rate versus the pre-amplifier gain for the six above mentioned power allocations.
The proposed Algorithm~\ref{algo:nl_maxsumcapa} is the best one and offers a gain, whatever the operating regime of the \acrshort{HPA}.
We remark that the solutions obtained when the optimization problems assume a linear \acrshort{HPA} are very bad and not suitable when the \acrshort{HPA} is in nonlinear regime.
Even worse, the lack of knowledge of the data rate expression in the nonlinear regime degrades the sum-rate, because we can wrongly think that increasing the gain of the pre-amplifier is beneficial for the sum-rate.
\begin{figure}[htbp]
	\centering
	\input{Figure/chap5_sumrate_vs_gain.tex}
	\caption{Sum-rate vs. pre-amplifier gain for $1$ \acrshort{FS} per $100\textnormal{ km}^2$.}
	\label{fig:chap5_sumrate_vs_gain}
\end{figure}

In Fig.~\ref{fig:chap5_sumrate_vs_density}, we plot the sum-rate versus the \acrshort{FS} density, for a pre-amplifier gain value set to $6$dB.
For high primary user density, we observe that the interference-temperature constraints prevails in the problem. 
Indeed, the sum-rates obtained with solutions $P^{\textnormal{li}}$ and $P^\star$ are close.
We can therefore say that taking into account the nonlinear interference is not necessary when the density of the \acrshort{FS} is high.
When the density of the \acrshort{FS} is low, the proposed $P^\star$ solution is then the best.
Indeed, the nonlinear effects are more important because the users transmit on average at a higher power.
\begin{figure}[htbp]
	\centering
	\input{Figure/chap5_sumrate_vs_density.tex}
	\caption{Sum-rate vs. \acrshort{FS} density for $G_\textnormal{amp}=6$dB.}
	\label{fig:chap5_sumrate_vs_density}
\end{figure}

\subsection{Minimum per-user data rate analysis}

We denote the power allocations related to the maximization of minimum per-user data rate by
\begin{itemize}
	\item $P^{\textnormal{naive1}}$, the power allocation where the users use the same transmit power $P$ which is then optimized for maximizing the minimum per-user data rate where the nonlinear interference is taken into account,
	
	\item $P^{\textnormal{naive2}}$, the power allocation where the received power at the satellite level is the same, so $G_m^{(b)} P_{b,m} = G_{m'}^{(b')} P_{b',m'}$, which is then optimized for maximizing the minimum per-user data rate where the nonlinear interference is taken into account,
	
	\item $P^{\textnormal{sota2}}$, the power allocation given by \cite{OPTI:lagunas2016} and detailed in Section~\ref{sec:chap3_fairness} where the (linear and nonlinear) interference is not taking into account,
	
	\item $P^{\textnormal{li}}$, the power allocation which is the optimal solution of Problem~\ref{pb:l_ibi_maxmin} obtained by numerical solver \cite{OPTI:cvx}, where only linear interference occurs, i.e. $\mathcal{P}_{b,m}^{(\textnormal{NL})} = 0$,
	
	\item $P^\star$, the power allocation which is the optimal solution of Problem~\ref{pb:nl_maxmin} obtained by numerical solver \cite{OPTI:cvx}.
\end{itemize}
In solid line, the minimum per-user data rate is evaluated with \eqref{eq:capa_nl} for nonlinearity-agnostic receiver.
In addition, we display in dotted line the minimum per-user data rate for the linear inter-beam interference case, i.e., when we enforce no nonlinear interference in the expression of the data rate \eqref{eq:capa_nl} and finally obtain the data rate \eqref{eq:sbd}.

In Fig.~\ref{fig:chap5_minrate_vs_gain}, we plot the minimum per-user data rate versus the pre-amplifier gain for the five above mentioned power allocations.
We observe that taking into account the nonlinear interference in the optimization problem allows to obtain higher minimum data rates.
\begin{figure}[htbp]
	\centering
	\input{Figure/chap5_minrate_vs_gain.tex}
	\caption{Minimum per-user data rate vs. pre-amplifier gain $G_\text{amp}$ for $1$ \acrshort{FS} per $100\textnormal{ km}^2$.}
	\label{fig:chap5_minrate_vs_gain}
\end{figure}

\subsection{Sum-power analysis}

We denote the power allocations related to the minimization of the sum-power by
\begin{itemize}
	\item $P^{\textnormal{naive1}}$, the power allocation where the users use the same transmit power $P$ which is then optimized for minimizing the sum-power where the nonlinear interference is taken into account,
	
	\item $P^{\textnormal{naive2}}$, the power allocation where the received power at the satellite level is the same, so $G_m^{(b)} P_{b,m} = G_{m'}^{(b')} P_{b',m'}$, which is then optimized for minimizing the sum-power where the nonlinear interference is taken into account,
	
	\item $P^{\textnormal{li}}$, the power allocation which is the optimal solution of Problem~\ref{pb:nl_minsumpower} when we force $\mathcal{P}_{b,m}^{(\textnormal{NL})} = 0$,
	
	\item $P^\star$, the power allocation which is the optimal solution of Problem~\ref{pb:nl_minsumpower} obtained by numerical solver \cite{OPTI:cvx}.
\end{itemize}
In solid line, the data rate is evaluated with \eqref{eq:capa_nl} for nonlinearity-agnostic receiver.
In addition, we display in dotted line the data rate for the linear inter-beam interference case, i.e., when we enforce no nonlinear interference in the expression of the data rate \eqref{eq:capa_nl} and finally obtain the data rate \eqref{eq:sbd}.

In Fig.~\ref{fig:chap5_sumpower_vs_rate}, we plot the sum-power versus the target data rate obtained for the four above mentioned power allocations.
We fix the same target data rate for all users, and we inspect two values for the pre-amplifier gain, denoted by $G_\text{amp}$.
We set the \acrshort{FS} density to $0.1$ per $\textnormal{km}^2$. 
We observe nonlinear effects only for a high target data rate, which is coherent because the minimization of the powers allows to stay in the linear zone of the \acrshort{HPA}.
We can conclude that the consideration of nonlinear interference for the minimization of the sum-power is not necessary.
\begin{figure}[htbp]
	\centering
	\input{Figure/chap5_sumpower_vs_rate.tex}
	\caption{Sum-power vs. target data rate ${R}_{b,m}^t = R^t$ with $G_\text{amp} \in \{-10,10\}$dB (respectively A and B), and $0.1$ \acrshort{FS} per $100\textnormal{ km}^2$.}
	\label{fig:chap5_sumpower_vs_rate}
\end{figure}

\section{Conclusion}\label{sec:chap5_conclusion}

In this chapter, we have addressed optimization problems in the context of a multibeam satellite with a nonlinear \acrshort{HPA} for nonlinearity-agnostic receiver.
More precisely, we formalized three optimization problems.
The first one maximizes the system sum-rate by taking into account the nonlinear interference.
The obtained problem boiled down to \acrshort{CGP} and we have proposed a \acrshort{SCA} procedure with monomial approximation.
The second problem maximizes the minimum per-user data rate.
After a rewriting, the resulting problem became naturally convex and the optimal solution was obtained thanks to numerical solver.
The last problem minimizes the sum-power of the system, subject to a target data-rate.
The rewriting of the constraints allowed to obtain a convex problem, which is solved in an optimal way by a numerical solver.
A summary of the addressed problems along with the proposed solutions is presented in Table~\ref{tab:chap5_conclusion}.

\begin{table}[htbp]
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		Issue & Problem & Solution \\ \hline
		Sum-rate & Problem~\ref{pb:nl_maxsumrate} & \acrshort{SCA} -- Algorithm~\ref{algo:nl_maxsumcapa} \\ \hline
		Fairness & Problem~\ref{pb:nl_maxmin} & CVX \\ \hline
		Sum-power & Problem~\ref{pb:nl_minsumpower} & CVX \\ 
		\hline
	\end{tabular}
	\caption{Addressed problems and proposed solutions.}
	\label{tab:chap5_conclusion}
\end{table}