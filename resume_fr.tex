\chapter*{Résumé}

Les communications par satellite sont une technologie prometteuse pour la connectivité omniprésente, étant donné la demande croissante des débits de données due aux nouvelles applications et aux nouveaux services.
Les normes de communication par satellite existantes, telles que DVB-S2X et sa liaison retour DVB-RCS2, peuvent déjà fournir des débits de données élevés.
Néanmoins, de nouveaux services et applications sont envisagés, tels que les communications par satellite à haut débit de nouvelle génération, l'internet des objets (IoT) et les communications de machine à machine (M2M).
Compte tenu du manque de techniques à fort impact pour augmenter les débits de données et de la rareté croissante du spectre, une stratégie pour améliorer les débits de données consiste à occuper une bande passante supplémentaire, ce qui entraîne un partage du spectre sous licence avec d'autres systèmes.
En effet, la majorité des systèmes existants utilisent des bandes de fréquences exclusives qui ne sont pas partagées avec d'autres entités.
C'est pourquoi la {\itshape World Radiocommunication Conference} (WRC) et des organisations similaires comme l'{\itshape European Telecommunications Standards Institute} (ETSI) et l' {\itshape International Telecommunication Union-Radiocommunication} (ITU-R) ont validé l'utilisation de nouvelles fréquences telles que $17.7$-$19.7$~GHz (liaison descendante par satellite) et $27.5$-$29.5$~GHz (liaison montante par satellite).
Cependant, ces fréquences sont déjà occupées par les systèmes terrestres en place, appelés \glsxtrfull{FS} et les systèmes par satellite doivent coexister avec eux de manière cognitive.

L'objectif de cette thèse est de se concentrer sur la liaison montante du satellite, afin de fournir de nouvelles solutions pour étendre le débit de données, en utilisant une bande passante supplémentaire non exclusive de $2$~GHz autour des fréquences $28$~GHz, déjà utilisées par les systèmes terrestres.
\textbf{Cet objectif conduit à la proposition de techniques d'allocation de ressources pour un système cognitif satellitaire de type sous-couche}.
Dans le paradigme de la radio cognitive de type sous-couche, les utilisateurs cognitifs doivent s'assurer que l'impact des interférences sur le système primaire en place ne dépasse pas les limites réglementaires d'interférence.
Dans cette thèse, les utilisateurs primaires sont le réseau terrestre en place (appelé \acrshort{FS}) et les utilisateurs secondaires sont les terminaux satellitaires, appelés \glsxtrfull{FSS}.
\textbf{Le problème abordé dans cette thèse est l'allocation des ressources (puissance et sous-bande) afin de satisfaire les contraintes de la radio cognitive.}
Les travaux proposés dans cette thèse couvrent cette problématique de liaison montante et permettent d'encourager le déploiement massif de terminaux cognitifs dans la bande $28$~GHz.
Plus précisément, la thèse se concentrera sur le scénario de liaison montante suivant.

Nous considérons un système de communication par satellite cognitif avec un opérateur et plusieurs terminaux satellitaires, avec des schémas d'accès orthogonaux et une réutilisation de fréquence à une seule couleur (c'est-à-dire en utilisant toute la bande de fréquence de $2$~GHz pour chaque faisceau).
Les terminaux satellitaires transmettent des données sur la liaison montante et interfèrent avec les récepteurs primaires terrestres voisins, car les lobes secondaires des antennes des terminaux ne peuvent pas être négligés.
Il est à noter qu'à l'intérieur d'un faisceau, les terminaux utilisent un schéma orthogonal basé sur \glsxtrfull{MF-TDMA}, comme dans la norme DVB-RCS2.

Dans ce scénario, le système de communication cognitif par satellite doit adapter sa puissance de transmission et l'allocation des sous-bandes, pour optimiser une fonction objective spécifique (par exemple, le débit, l'équité, ...), tout en satisfaisant les contraintes d'interférence concernant les systèmes primaires.
Comme mentionné ci-dessus, le but de cette thèse est d'améliorer le débit des utilisateurs secondaires tout en limitant les interférences causées aux utilisateurs primaires.
En général, l'allocation des sous-bandes et des puissances est entreprise, afin de limiter les interférences avec les utilisateurs primaires.

En outre, les \glsxtrfull{HPA} à large bande sont utilisés à bord des satellites en raison des contraintes de puissance/masse, et une multitude de sous-bandes (ou porteuses) sont amplifiées conjointement par un \acrshort{HPA}.
Le \acrshort{HPA} est intrinsèquement non linéaire et l'amplification de plusieurs sous-bandes provoque des distorsions dans la bande et hors de la bande.
Cela réduit le \glsxtrfull{SINR} de la sous-bande, ce qui entraîne une baisse du débit.

Un back-off plus élevé ou un espacement plus important entre les sous-bandes sont nécessaires pour réduire les distorsions susmentionnées, ce qui entraîne une efficacité spectrale et un rendement moins bons.
Ainsi, la prise en compte du \acrshort{HPA} ajoute des sources supplémentaires de complexité, lorsqu'on cherche à optimiser la conception du système.
L'allocation traditionnelle des ressources, basée sur des canaux d'interférence linéaires, peut ne pas convenir à de tels canaux d'interférence non linéaires, ce qui nécessite une étude plus approfondie.
Le principal défi de cet exercice comprend l'évaluation des effets non linéaires sur le débit des utilisateurs, lorsque le \acrshort{HPA} est modélisé par des séries de Volterra.

\textbf{Par conséquent, l'objectif principal de la thèse est d'effectuer l'allocation des ressources (puissance et sous-bande) afin de maximiser le débit total du système (ou d'autres fonctions objectives pertinentes), tout en satisfaisant les limites d'interférence concernant les systèmes primaires, en prenant en compte les dégradations non linéaires dues aux composants du satellite modélisés par des séries de Volterra.}

Cette thèse est composée de six chapitres.
Les principales contributions sont présentées dans les chapitres~\ref{chap:l}, \ref{chap:capa}, \ref{chap:nl} et \ref{chap:lnl}, tandis que le chapitre~\ref{chap:problem_statement} présente le contexte de la recherche et le chapitre~\ref{chap:sp} donne un aperçu des outils d'optimisation utilisés tout au long de la thèse.

Dans le chapitre~\ref{chap:problem_statement}, nous avons présenté le modèle de notre système de communication par satellite, de l'utilisateur terrestre jusqu'à la passerelle, en passant par le satellite.
Nous avons décrit également le réseau primaire et la contrainte d'interférence associée, que le réseau satellitaire cognitif ne doit surtout pas dépasser.
Ensuite, le problème général d'optimisation a été énoncé et nous avons ouvert la discussion concernant les objectifs de la thèse.
	
Dans le chapitre~\ref{chap:sp}, nous avons introduit le concept d'optimisation convexe et donné des méthodes conventionnelles de programmation non linéaire pour résoudre les problèmes non convexes.
Nous nous sommes concentrés également sur une branche de l'optimisation, appelée \glsxtrfull{GP} et son extension non-convexe \glsxtrfull{SP}.
En particulier, nous avons présenté une méthode générique pour résoudre le problème non convexe \acrshort{SP}.
Les outils présentés dans ce chapitre ont largement été utilisés dans les chapitres suivants, qui traitent des problèmes d'allocation de ressources.
	
Dans le chapitre~\ref{chap:l}, nous avons abordé le problème d'optimisation avec l'hypothèse d'un \acrshort{HPA} linéaire, en se focalisant sur l'allocation de puissance.
Même si ce scénario semblait basique, les problèmes d'optimisation rencontrés ont dû tout de même prendre en compte l'interférence inter-faisceaux, conduisant à des problèmes non convexes.
Cela nous a permis d'appliquer et de comprendre les procédures de résolution des problèmes non convexes, présentées dans le chapitre~\ref{chap:sp}. 
Nous avons proposé également deux méthodes d'allocation pour l'attribution des sous-bandes et l'allocation de puissance.
	
Dans le chapitre~\ref{chap:capa}, nous avons exprimé le débit de données des utilisateurs en présence d'effets non linéaires générés par le \acrshort{HPA}, à partir de l'expression de l'information mutuelle dans le cas d'un canal non linéaire.
Le \acrshort{HPA} a été modélisé comme un polynôme sans mémoire, conduisant à des séries de Volterra.
Nous avons obtenu deux expressions du débit de données des utilisateurs, liées à deux récepteurs différents.
Le premier, appelé {\itshape nonlinearity-aware receiver}, exploitait les effets non linéaires comme information supplémentaire.
L'autre, appelé {\itshape nonlinearity-agnostic receiver}, considérait les non-linéarités comme un bruit additionnel.
Enfin, nous avons mis l'accent sur les propriétés mathématiques des expressions du débit de données.
	
Dans le chapitre~\ref{chap:nl}, nous avons abordé le problème de l'optimisation en présence d'effets non linéaires et en utilisant le récepteur voyant les non-linéarités comme de l'interférence.
Sur la base de l'expression du débit de données correspondante, nous avons montré que les problèmes rencontrés ont pu être exprimés sous forme \acrshort{GP} ou \glsxtrfull{CGP} et nous avons présenté une méthode de résolution utilisant les outils donnés dans le chapitre~\ref{chap:sp}.
	
Dans le chapitre~\ref{chap:lnl}, nous avons considéré le problème d'optimisation en présence d'effets non linéaires et en utilisant le récepteur exploitant les non-linéarités.
À partir de l'expression du débit évaluée avec ce récepteur, on avons obtenu des problèmes de la forme \acrshort{CGP} ou \acrshort{SP}.
Pour résoudre ces problèmes non convexes, nous avons présenté des méthodes de résolution utilisant les outils donnés dans le chapitre~\ref{chap:sp}.
