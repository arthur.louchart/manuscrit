\chapter*{General introduction}
\addcontentsline{toc}{chapter}{General introduction}

\section*{Context and objectives of the thesis}

Satellite-based communication is a promising technology for ubiquitous connectivity, given the increasing demand for data rate due to new applications (3DTV/ UHDTV, broadband internet) and services (aeronautical / maritime).
Existing satellite communications standards, such as DVD-S2X (and its return link DVB-RCS2), can already deliver high data rates for TV and broadband applications.
Yet, new services and applications are envisioned: for instance, one can mention SES-17 and Eutelsat QUANTUM, both providing enhanced flexibility, capacity, coverage, and service quality for the highly competitive mobility market, as well as other fast growing enterprise markets.
Next generation high throughput broadband satellite communications, internet of things (IoT), and machine to machine (M2M) communications are also examples.
However, the majority of existing systems use exclusive spectrum bands that are not shared with other entities.
With a lack of high-impact techniques to increase data rates and increasing spectrum scarcity, one proposed strategy to improve data rates is to occupy additional bandwidth, resulting in a share of licensed spectrum with other systems.
Therefore {\itshape World Radiocommunication Conference}, and similar organizations like European Telecommunications Standards Institute (ETSI), and International Telecommunication Union-Radiocommunication Sector (ITU-R) have validated the use of new frequencies such as $17.7$-$19.7$~GHz (satellite downlink) and $27.5$-$29.5$~GHz (satellite uplink).
However, these frequencies are already occupied by incumbent terrestrial systems, called \glsxtrfull{FS} systems, and the satellite-based systems must coexist with them in a cognitive manner.

The aim of this thesis is to focus on the satellite uplink in order to provide new solutions for extending the data rate by using additional non-exclusive $2$~GHz bandwidth in $28$~GHz, and consequently opening new areas of applications and services in satellite communications on frequencies already used by terrestrial systems.
\textbf{This goal leads to the proposed resource allocation techniques for an underlay satellite cognitive systems} \cite{COGNITIF:blasco2012}.
Indeed, the initial studies of the European Project CoRaSat showed that the cognitive underlay paradigm suits well for deployment of cognitive satellite terminals in uplink \cite{SATCOM:maleki2015,OPTI:lagunas2015}.
These studies have shown that employing cognitive radio paradigm in satellite communications can increase the uplink system throughput.
In an underlay cognitive radio, the cognitive users have to ensure that the impact of interference on the incumbent system does not exceed the regulatory interference limitations.
As in \cite{SATCOM:maleki2015,COGNITIF:tarchi2014,COGNITIF:liolis2013}, the cognitive radio principle applied in this thesis corresponds to the case where the primary user is the incumbent terrestrial network (i.e., the \acrshort{FS}) and the secondary users are the satellite terminals of interest, so called \glsxtrfull{FSS}.
\textbf{The issue addressed in this thesis is the allocation of resources (power and subband) in order to fulfill the cognitive radio constraints.}
Within the CoRaSat project, efficient techniques have been developed for resource allocation for cognitive downlink satellite communications \cite{OPTI:lagunas2015, COGNITIF:sharma2015, COGNITIF:sharma2015_icc}.
However, as it will be discussed later, cognitive uplink satellite communications still is in its infancy and some works were proposed in \cite{OPTI:lagunas2015,OPTI:lagunas2016}.
The work proposed in this thesis covers that uplink issue and allows to encourage mass deployment of cognitive uplink terminals in $28$~GHz band.
More specifically, the thesis will focus on the following relevant uplink scenario.

We consider a cognitive radio network with one operator and multiple satellite terminals with orthogonal access schemes and single color frequency reuse (i.e. using the whole $2$~GHz frequency band for each beam).
The satellite terminals transmit data on the uplink and interfere with their neighboring incumbent terrestrial receivers since the sidelobes of the terminal can not be neglected.
Note that within a beam, the terminals employ an orthogonal scheme based on \glsxtrfull{MF-TDMA} as in DVB-RCS2 standard \cite{OPTI:lagunas2015}.

In that scenario, the cognitive radio satellite system has to adapt its power and the subband allocation to optimize a specific objective function (e.g. throughput, fairness,
etc.) while satisfying the interference temperature constraints regarding the primary systems.
As mentioned above, the aim of this thesis is to enhance the throughput of the secondary users while limiting the interference to the primary users within acceptable limits.
Typically, subband and power allocation is undertaken, based on available information, to limit interference to the primary users.

Additionally, wideband \glsxtrfull{HPA} are being used on-board of satellite due to power/mass constraints and a multitude of subbands (or carriers) are jointly amplified by a \acrshort{HPA}.
The \acrshort{HPA} is intrinsically nonlinear and amplification of multiple subbands causes in-band and out-of-band distortions.
This reduces the \glsxtrfull{SINR} of the subband leading to lower throughput and is affected by resource allocation (especially subband and power allocation).

Higher back-off or higher subband spacing are needed to reduce the aforementioned distortions, leading to poorer \acrshort{HPA} efficiency and spectral use.
Poorer \acrshort{HPA} efficiency affects payload dimensioning due to scarce power on-board and increases operational costs of the uplink terminal, the inefficient spectral efficiency belies the cognitive paradigm.
Thus including the \acrshort{HPA} adds additional sources of complexity when aiming for optimized system design.
Traditional resource allocation based on linear interference channels may not be suitable for such nonlinear interference channels leading to further investigation.
The key challenge in this exercise includes the evaluation of nonlinear effects on user throughput, when the \acrshort{HPA} is modeled by Volterra series.

\textbf{Therefore, the main objective of the thesis is to perform resource allocation (power and subband) to maximize the system sum-rate (or other relevant objective functions), while satisfying the interference temperature limits regarding the primary systems, by taking into account nonlinear impairments due to satellite components modeled by Volterra series.}

\section*{Outline of the manuscript and contributions}

This thesis is composed of six chapters. The main contributions are presented in Chapters~\ref{chap:l}, \ref{chap:capa}, \ref{chap:nl} and \ref{chap:lnl}, while Chapter~\ref{chap:problem_statement} presents the context of the research and Chapter~\ref{chap:sp} provides an overview of the optimization tools used throughout the thesis.

In Chapter~\ref{chap:problem_statement}, the system model of the satellite communication is presented, from the terrestrial user to the gateway, by passing through the satellite.
We also describe the primary network and the relevant constraint of interference-temperature, that the cognitive satellite network has to not exceed.
Then the general optimization problem is stated and finally we open the discussion regarding the goals of the thesis.

In Chapter~\ref{chap:sp}, we introduce the concept of convex optimization and give conventional nonlinear programming methods to solve non-convex problems.
We also focus on a branch of optimization, which is called \glsxtrfull{GP} and its non-convex extension \glsxtrfull{SP}.
In particular, we present a generic method to solve non-convex \acrshort{SP} problem.
The tools presented in this chapter are widely used in the following chapters dealing with resource allocation problems.

In Chapter~\ref{chap:l}, we address the optimization problem with the assumption of a linear \acrshort{HPA} focusing on power allocation.
Even if this scenario seems basic, the optimization problems encountered must still take into account the inter-beam interference, leading to non-convex problems.
This allows us to apply and understand the resolution procedures for non-convex problems presented in Chapter~\ref{chap:sp}. 
We also propose two allocation methods for subband assignment and power allocation.

In Chapter~\ref{chap:capa}, we express the user data rate in the presence of nonlinear effects generated by the \acrshort{HPA}, from the expression of the mutual information in the case of a nonlinear channel.
The \acrshort{HPA} is modeled as a memoryless polynomial, leading to Volterra series.
We obtain two expressions of the user data rate, related to two different receivers.
The first one, called {\itshape nonlinearity-aware receiver}, exploiting the nonlinear effects as additional information.
The other one, called {\itshape nonlinearity-agnostic receiver}, considering the nonlinearities as additional noise.
We finally emphasize the mathematical properties of data rate expressions.

In Chapter~\ref{chap:nl}, we address the optimization problem in the presence of nonlinear effects and using the nonlinearity-agnostic receiver.
Based on the corresponding data rate expression, we show that the problems encountered can be expressed in \acrshort{GP} or \glsxtrfull{CGP} form and we present a taylored resolution method derived from the general framework as given in Chapter~\ref{chap:sp}.

In Chapter~\ref{chap:lnl}, we consider the optimization problem in the presence of nonlinear effects and using the nonlinearity-aware receiver.
From the expression of the data rate evaluated with this receiver, we obtain problems of the form \acrshort{CGP} or \acrshort{SP}.
To solve these non-convex problems, we present taylored resolution methods derived from the general framework as given in Chapter~\ref{chap:sp}.

\newpage

\section*{List of publications}

The research work during the thesis has produced the following publications.

\subsection*{Peer-reviewed journal}

\begin{itemize}
	\item[\textbf{J1.}] A. Louchart, P. Ciblat, and C. Poulliat, "Power Allocation for Uplink Multiband Satellite Communications With Nonlinear Impairments," in IEEE Communications Letters, vol. 25, no. 8, pp. 2713-2717, Aug. 2021.
	
	\item[\textbf{J2.}] A. Louchart, E. Tohidi, P. Ciblat, D. Gesbert, E. Lagunas, and C. Poulliat, "Power allocation for multi-operator and multi-beam cognitive uplink satellite systems," {\itshape To be submitted for IEEE Transactions on Cognitive Communications and Networking}.
\end{itemize}

\subsection*{International conference}

\begin{itemize}
	\item[\textbf{C1.}] A. Louchart, P. Ciblat, and P. de Kerret, "Resource Optimization for Cognitive Satellite Systems with Incumbent Terrestrial Receivers," {\itshape European Signal Processing Conference (EUSIPCO)}, A Coru\~na, Spain, 2019, pp. 1-5.
	
	\item[\textbf{C2.}] A. Louchart, P. Ciblat, and C. Poulliat, "Sum-capacity of Uplink Multiband Satellite Communications with Nonlinear Impairments," {\itshape IEEE International Conference on Communications (ICC)}, Montreal, Canada, 2021, pp. 1-6.
	
	\item[\textbf{C3.}] A. Louchart, P. Ciblat, and C. Poulliat, "Power allocation in Uplink Multiband Satellite System with Nonlinearity-Aware Receiver," {\itshape IEEE Signal Processing Advances in Wireless Communications (SPAWC)}, Lucca, Italy, 2021, pp. 1-5.
	
	\item[\textbf{C4.}] A. Louchart, P. Ciblat, and C. Poulliat, "Power Allocation for Multibeam Satellite Communications with Nonlinear Impairments," {\itshape ITG Workshop on Smart Antennas (WSA)}, Sophia Antipolis, France, 2021, pp. 1-6.
\end{itemize}

\subsection*{French conference}

\begin{itemize}
	\item[\textbf{C5.}] A. Louchart, P. de Kerret, and P. Ciblat, "Allocation de ressources pour des réseaux satellitaires cognitifs," {\itshape GRETSI}, Lille, France, 2019, pp. 1-4.
\end{itemize}