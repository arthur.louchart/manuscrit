\chapter{Problem statement}\label{chap:problem_statement}

Traditionally, most of the existing communication systems operate on exclusive spectrum bands which are not shared with other entities.
Due to spectrum scarcity and the dearth of high-impact techniques to enhance data rate, a promising approach is to extend the usable spectrum by considering operation in the non-exclusive bands.
In that context, {\itshape World Radiocommunication Conference}, European Telecommunications Standards Institute (ETSI), and International Telecommunication Union-Radiocommunication Sector (ITU-R) have validated the co-primary use of certain spectrum portions in the Ka-band, i.e., 27.5-29.5~GHz for the satellite uplink \cite{COGNITIF:evans2015}. 
These frequencies are already occupied by incumbent terrestrial systems, called \glsxtrfull{FS} systems, which implies that the upcoming satellite-based systems will have to coexist with them in an underlay \glsxtrfull{CR} manner. 

The cognitive users have thus to ensure that the impact of interference on the incumbent system does not exceed the regulatory interference limitations.
In the particular instance of the satellite communication \acrshort{CR} system, the primary user is the incumbent terrestrial network, i.e. the \acrshort{FS}, and the secondary users are the satellite terminals of interest, so-called \glsxtrfull{FSS} users.
As in more traditional terrestrial \acrshort{CR} systems, one central issue to facilitate the coexistence between the \acrshort{CR} devices and the incumbent is the {\itshape power allocation} in order to fulfill the cognitive radio constraints and to increase the data rate of the secondary systems.
The satellite communication \acrshort{CR} systems described in Fig.~\ref{fig:scenario} consider orthogonal access schemes within multiple beams, single color frequency reuse (i.e. all the beams use the same bandwidth) and take into account \glsxtrfull{HPA} at the Geostationary (GSO) satellite side.
Moreover, the created interference from satellite terminals due to antenna sidelobes can affect multiple \acrshort{FS} receivers, which are not mobile.

This first chapter presents the system model and the considered transmission chain and formalizes the main optimization problem to be solved.
All the notions and notations presented in this chapter constitute the foundations for the rest of this thesis.

The rest of the chapter is organized as follows. 
In Section~\ref{sec:system_model}, we describe the cognitive satellite communication system model.
In Section~\ref{sec:transmission_chain}, we give the mathematical writing of the transmission chain of the considered system.
In Section~\ref{sec:problem_statement}, the main optimization problem is formulated, and the constraints of the problem are made explicit.
Finally, Section~\ref{sec:conclusion_chap1} concludes this chapter.

\section{System model of satellite communication}\label{sec:system_model}

As in DVB-RCS2 standard for uplink satellite communication, we consider a \glsxtrfull{MF-TDMA}.
It allows a group of user terminals on the Earth to communicate with the gateway (through the satellite) by means of a time-frequency resource grid.
Essentially, a set of carrier frequencies is considered, each of which is divided into time-slots.
For the sake of synchronization aspects, fixed-slot \acrshort{MF-TDMA} is usually considered, where the bandwidth and duration of successive traffic slots used by a particular terminal are fixed.
In this thesis, we work on the power allocation within one time-slot assuming that the subband assignment has been already fixed.
We consider a frequency reuse factor between each beam equal to one.
Each beam of the satellite uses the same \acrshort{MF-TDMA} scheme synchronized between them.
If not synchronized, we may add  a random time but the way to write the interference power between beams will be similar and does not modify the structure of the optimization problem, just its numerical evaluation.
Therefore, for the sake of simplicity, we assume perfect synchronization.

We consider a multibeam satellite system where $N$ terrestrial users are spread over $B$ beams using the same band, so there are $K:=N/B$ users in a beam. This band is split into $M$ subbands.
In each beam, we assume a \glsxtrfull{FDMA} preventing the intra-beam interference, where the number of users $K$ is equal to the number of subband, so $M=K$. 
User using subband $m$ in beam $b$ will transmit a symbol sequence $\{a_{b,m,n}\}_{n}$, where $n$ is the symbol index.
All users have the same shaping filter with an impulse response of $p_T(t)$.
This shaping filter $p_T(t)$  is an \glsxtrfull{SRRC} with a roll-off of $0.25$.
The DVB-RCS2 standard allows to use others values.

\begin{figure}[htbp]
	\centering
	\def\svgwidth{0.75\textwidth}
	\input{Figure/scenario.pdf_tex}
	\caption{Scenario of the satellite communication system which creates interference on the primary network.}
	\label{fig:scenario}
\end{figure}

\section{Transmission chain of satellite communication}\label{sec:transmission_chain}

In Fig.~\ref{fig:transmission_chain}, we display a diagram of the transmission chain of our satellite communication system.

\begin{figure}[htbp]
	\centering
	\begin{adjustbox}{max width=\textwidth}
	\input{Figure/block-diagram.tex}
	\end{adjustbox}
	\caption{Transmission chain of our satellite communication system with $B=2$ beams and $M=2$ users per beam. The dotted lines represent the inter-beam interference.}
	\label{fig:transmission_chain}

\end{figure}

The baseband signal emitted by the user belonging to beam $b$ using subband $m$, denoted by $x_{b,m}(t)$, is  
\begin{equation}\label{eq:baseband_signal}
x_{b,m}(t) = \sum_{n\in \mathbb{Z}} a_{b,m,n} p_T(t - n T_s).
\end{equation}

Each signal $x_{b,m}(t)$ is transposed around the frequency $f_m$. The difference between two adjacent frequencies is denoted $\Delta F$.

The antenna $b$ associated with beam $b$ receives the sum of the $M$ transposed signals of this beam and the inter-beam interference. This received analytic signal is denoted by $x_\textnormal{a}^{(b)}(t)$,
\begin{equation}
x_\textnormal{a}^{(b)}(t) =\sum_{m=1}^{M} \sqrt{G_{m}^{(b)}} x_{b,m}(t) e^{2i \pi f_m t} + x_\text{IB}^{(b)}(t),
\end{equation}
where $x_\text{IB}^{(b)}(t)$ is the inter-beam interference,
\begin{equation}
x_\text{IB}^{(b)}(t) = \sum_{\substack{b'=1 \\ b' \ne b}}^{B} \sum_{m=1}^{M} \sqrt{G_{m}^{(b',b)}} x_{b',m}(t) e^{2i \pi f_m t},
\end{equation}
with 
\begin{itemize}
	\item $G_{m}^{(b)}$ the channel gain between user using subband $m$ of beam $b$ and antenna $b$,
	\item  $G_{m}^{(b',b)}$ the channel gain between user using subband $m$ of beam $b'$ and antenna $b$.
\end{itemize}

Let $y_\textnormal{a}^{(b)}(t)$ be the received analytic signal at the gateway coming from the antenna $b$.
From the received signal at the antenna $b$ $x_\textnormal{a}^{(b)}(t)$, the received signal at the gateway passes through a dedicated \acrshort{HPA} modeled as nonlinear memoryless device and an \glsxtrfull{AWGN} channel between the satellite and the gateway. Consequently, we get 
\begin{equation}\label{eq:io}
y_\textnormal{a}^{(b)}(t) = \gamma_1 x_\textnormal{a}^{(b)}(t) + \gamma_3 x_\textnormal{a}^{(b)}(t) x_\textnormal{a}^{(b)}(t) \overline{x_\textnormal{a}}^{(b)}(t) + w_\textnormal{a}(t),
\end{equation}
where $\overline{\ \cdot \ }$ stands for the complex-conjugate, and $w_\textnormal{a}(t)$ is a complex-valued circularly-symmetric zero-mean \acrshort{AWGN} with variance $\mathcal{P}_\text{W}$.
The coefficients $\gamma_1$ and $\gamma_3$ are positive parameters and characterize the nonlinear distortion of the \acrshort{HPA} \cite{SATCOM:beidas2011}.
Notice that the input-output relationship of the \acrshort{HPA} is expressed as a power series truncated at third order, which is commonly used in satellite communication context \cite{SATCOM:beidas2011,SATCOM:benedetto1978}.
Moreover, we assume a perfect link between the satellite and the gateway, because the downlink uses a different frequency and involves broadcast to a single gateway.

Let us now consider the demodulation for user belonging to beam $b$ using subband $m$. We first go back in baseband, 
\begin{equation}\label{eq:y}
y_{b,m}(t) = y_\textnormal{a}^{(b)}(t) e^{-2i \pi f_m t },
\end{equation}
we then apply the matched filter $p_R(t):=\overline{p_T}(-t)$, 
\begin{equation}\label{eq:z}
z_{b,m}(t) = \int_{\mathbb{R}}  p_R(\tau) y_{b,m}(t - \tau)  d\tau.
\end{equation}
Finally, the signal is sampled at the symbol rate $T_s$ resulting in the sequence $z_{b,m,n}$,
\begin{equation}
z_{b,m,n} = z_{b,m}(n T_s).
\end{equation}

To conduct the derivations of $z_{b,m,n}$, we first write $z_{b,m}(t)$ according to the symbol sequences,
\begin{align}
z_{b,m}(t) & =  \gamma_1  \sum_{m'=1}^{M} \sum_{n' \in \mathbb{Z}} \sqrt{G_{m'}^{(b)}} a_{b,m',n'} e^{2 i \pi (f_{m'} - f_m)  t} \int_{\mathbb{R}} p_T(t - \tau - n' T_s) p_R(\tau) e^{-2i \pi (f_{m'} - f_m)   \tau} d \tau \nonumber \\
& + \gamma_1  \sum_{\substack{b'=1 \\ b' \ne b}}^{B} \sum_{m'=1}^{M} \sum_{n' \in \mathbb{Z}} \sqrt{G_{m'}^{(b',b)}} a_{b',m',n'} e^{2 i \pi (f_{m'} - f_m)  t} \int_{\mathbb{R}} p_T(t - \tau - n' T_s) p_R(\tau) e^{-2i \pi (f_{m'} - f_m)   \tau} d \tau \nonumber \\
& + \gamma_3 \sum_{b_1, b_2, b_3=1}^{B} \sum_{m_1, m_2, m_3=1}^{M} \sum_{n_1, n_2, n_3 \in \mathbb{Z}} \sqrt{G_{m_1}^{(b_1,b)} G_{m_2}^{(b_2,b)} G_{m_3}^{(b_3,b)}} \nonumber \\
&\times  a_{b_1,m_1,n_1} a_{b_2,m_2,n_2} \overline{a_{b_3,m_3,n_3}} e^{2i \pi (f_{m_1} + f_{m_2} - f_{m_3} - f_m)  t} \nonumber \\
&\times  \int_{\mathbb{R}} p_T(t - \tau - n_1 T_s) p_T(t - \tau - n_2 T_s) p_T(t - \tau - n_3 T_s) p_R(\tau) e^{-2i \pi (f_{m_1} + f_{m_2} - f_{m_3} - f_m)   \tau} d \tau \nonumber \\
& + \int_{\mathbb{R}}  p_R(\tau) w_\textnormal{a}(t-\tau )e^{-2i\pi f_m (t-\tau)} d\tau.
\end{align}
Notice that the perfect synchronization between beams is assumed. This hypothesis is realistic because the beams are collocated at the satellite.

In order to simplify the writing of $z_{b,m}(t)$, two Volterra kernels of first-order and third-order respectively are introduced:
\begin{align}
h_1(t_1, \ell) & = \int_{\mathbb{R}} p_T(t_1 - \tau) p_R(\tau) e^{-2 i \pi \ell \Delta F  \tau} d \tau,\\
h_3(t_1, t_2, t_3, \ell) & = \int_{\mathbb{R}} p_T(t_1 - \tau) p_T(t_2 - \tau) p_T(t_3 - \tau) p_R(\tau) e^{-2 i \pi \ell \Delta F  \tau} d \tau.
\end{align}

Therefore,
\begin{align}
z_{b,m}(t) & = \gamma_1  \sum_{m'=1}^{M} \sum_{n' \in \mathbb{Z}} \sqrt{G_{m'}^{(b)}} a_{b,m',n'} e^{2 i \pi ({m'} - m) \Delta F  t} h_1(t-n'T_s,m'-m) \nonumber \\
& + \gamma_1  \sum_{\substack{b'=1 \\ b' \ne b}}^{B}\sum_{m'=1}^{M} \sum_{n' \in \mathbb{Z}} \sqrt{G_{m'}^{(b',b)}} a_{b',m',n'} e^{2 i \pi ({m'} - m) \Delta F  t} h_1(t-n'T_s,m'-m) \nonumber \\
& + \gamma_3 \sum_{b_1,b_2,b_3 = 1}^{B} \sum_{m_1,m_2,m_3=1}^{M} \sum_{n_1,n_2,n_3 \in \mathbb{Z}} \sqrt{G_{m_1}^{(b_1,b)} G_{m_2}^{(b_2,b)} G_{m_3}^{(b_3,b)}} \nonumber \\
& \times a_{b_1,m_1,n_1} a_{b_2,m_2,n_2} \overline{a_{b_3,m_3,n_3}} e^{2i \pi ({m_1} +{m_2} -{m_3} -m)  \Delta F t} \nonumber \\
& \times  h_3(t - n_1 T_s, t - n_2 T_s,t - n_3 T_s, m_1+m_2-m_3-m) \nonumber \\
& + \int_{\mathbb{R}}  p_R(\tau) w_\textnormal{a}(t-\tau )e^{-2i\pi f_m (t-\tau)} d\tau.
\end{align}

Consequently, after sampling the signal $z_{b,m}(t)$ at $nT_s$, the term $z_{b,m,n}$ can be decomposed into four parts:
\begin{equation}\label{eq:z_decomposition}
z_{b,m,n}=z_{b,m,n}^{(\mathrm{L})}+ z_{b,m,n}^{(\mathrm{I})}+ z_{b,m,n}^{(\mathrm{NL})} + w_{b,m,n},
\end{equation}
where $z_{b,m,n}^{(\mathrm{L})}$ is the part depending on the current symbol, $z_{b,m,n}^{(\mathrm{I})}$ the part depending linearly on the symbols $\{a_{b,m,n}\}$ except the current one, and $z_{b,m,n}^{(\mathrm{NL})}$ the part depending non-linearly on the symbols $\{a_{b,m,n}\}$.

As $h_1(nT_s,m)$ is equal to zero for any $n\neq 0$ or any $m\neq 0$ (orthogonality in time and frequency between users thanks to the \acrshort{SRRC} shaping filter which fulfills the Nyquist criterion), and equal to one otherwise, we force $m' = m$ and $n' = n$ to obtain the linear part as follows
\begin{equation}\label{eq:z_L}
z_{b,m,n}^{(\mathrm{L})} = \gamma_1 \sqrt{G_{m}^{(b)}} a_{b,m,n},
\end{equation}
\begin{equation}\label{eq:z_I}
z_{b,m,n}^{(\mathrm{I})} = \gamma_1 \sum_{\substack{b'=1 \\ b' \ne b}}^{B} \sqrt{G_{m}^{(b',b)}} a_{b',m,n}.
\end{equation}
The non-linear part takes the following form
\begin{align}\label{eq:z_NL}
z_{b,m,n}^{(\mathrm{NL})} & = \gamma_3 \sum_{b_1,b_2,b_3 = 1}^{B} \sum_{m_1,m_2,m_3=1}^{M} \sum_{n_1,n_2,n_3 \in \mathbb{Z}} \sqrt{G_{m_1}^{(b_1,b)} G_{m_2}^{(b_2,b)} G_{m_3}^{(b_3,b)}}\nonumber\\
& \times a_{b_1,m_1, n - n_1} a_{b_2,m_2,n - n_2} \overline{a_{b_3,m_3, n-n_3}} e^{2i \pi (m_1+m_2 - m_3-m) \Delta F n T_s}  \nonumber \\
& \times h_3(n_1 T_s,n_2 T_s,n_3 T_s, m_1+m_2-m_3-m). 
\end{align}

Throughout the thesis, we will assume that the received samples sequence of user belonging to beam $b$ using subband $m$ follows \eqref{eq:z_decomposition}-\eqref{eq:z_NL}.
When we assume a perfect \acrshort{HPA}, so only linearity is considered as done in Chapter~\ref{chap:l}, we set the \acrshort{HPA} coefficient to $\gamma_1=1$ and $\gamma_3=0$.
Otherwise in Chapters~ \ref{chap:capa}, \ref{chap:nl} and \ref{chap:lnl}, we have $\gamma_1\neq 1$ and $\gamma_3\neq 0$. 

\section{General statement of the optimization problem}\label{sec:problem_statement}

We consider that the satellite communication \acrshort{CR} system adjusts its transmission strategy, i.e. its transmit power, with the goal of maximizing its own sum data rate while not causing harmful interference to the primary services \cite{OPTI:lagunas2015,COGNITIF:an2019}.
The reason behind this approach is that spectrum-hungry applications for satellite communication are broadband services, which demand higher data rate.
As a consequence, the main optimization problem to be solved in this thesis is the maximization of the system sum rate.

It is well-known that maximizing data rate ignores fairness among different users.
Fairness objectives have been proposed in the literature to avoid such undesirable situations.
Most of the works have considered fairness by focusing on maximizing the minimum per-user data rate or the sum-log data rate of the cognitive user \cite{OPTI:zhao,OPTI:lagunas2016}.
This is why the fairness between users is additionally addressed, as well as other figures of merit will be considered from time to time.

Before focusing on the cost function to maximize, we first study the constraints. 
First of all, we need to limit the interference power received at each terrestrial incumbent (\acrshort{FS}) receivers. We assume $L$ primary \acrshort{FS} receivers. 
As in \cite{OPTI:lagunas2015}, we assume that each primary receiver works on a set of band intervals where each band interval corresponds to the set of $S$ adjacent subbands of the satellite communication \acrshort{CR} system. We put $T=M/S$ the number of \acrshort{FS} subbands.
For the sake of simplicity, we force $T$ to be an integer.
We define $\mathcal{S}_{m'} = \{ (m'-1)S+1, \cdots, m' S\}$ the set of \acrshort{FSS} subbands inside the \acrshort{FS} subband $m'$.
On each band interval $m'\in\{1, \dots, T\}$ for each \acrshort{FS} receiver $\ell\in\{1, \dots, L\}$, we have to satisfy the following interference-temperature constraints:
\begin{equation}\label{eq:contrainteFS}
\sum_{b=1}^B  \sum_{m \in \mathcal{S}_m'} F_{b,m}^{(\ell)} P_{b,m} \leq I_{th}^{(\ell)}(m'), \quad \forall \ell, m',
\end{equation}
with 
\begin{itemize}
	\item $I_{th}^{(\ell)}(m')$ the interference-temperature at \acrshort{FS} $\ell$ on band interval $m'$ that the satellite communication \acrshort{CR} system has to satisfy,
	\item $P_{b,m}:=\mathbb{E}\left[\left|a_{b,m,n}\right|^2\right]$ the power of user belonging to beam $b$ using subband $m$,
	\item $F_{b,m}^{(\ell)}$ the channel gain between user belonging to beam $b$ using subband $m$ to \acrshort{FS} receiver $\ell$.
\end{itemize}

In addition, for each user, we have a peak power constraint, i.e., 
\begin{equation}\label{eq:contrainteP}
0\leq P_{b,m}\leq P_{\max}, \quad \forall b,m,
\end{equation}
where $P_{\max}$ is the maximum transmit power.

We now move the general optimization problem corresponding to maximize the sum data rate of the whole satellite communication \acrshort{CR} systems, while satisfying the interference-temperature and individual power constraints.
So we have Problem~\ref{pb:main} where $\mathbf{P} = \left( P_{b,m} \right)_{1\le b \le B, 1 \le m \le M}$ is the matrix formed by the transmit power of users, and $R_{b,m}$ is the data rate for user using subband $m$ belonging to beam $b$. 

\begin{problem}[Main Problem]\label{pb:main}
	\begin{align}
	\mathbf{P}^{\star} = & \arg\max_{ \mathbf{P} } \sum_{b=1}^B\sum_{m=1}^M   R_{b,m} \\
	\textnormal{s.t. } & (\ref{eq:contrainteFS}) \textnormal{ and } (\ref{eq:contrainteP}) \nonumber. 
	\end{align}
\end{problem} 

The way to express the data rate $R_{b,m}$ depends on the chapter, since it is based on the assumptions made about the \acrshort{HPA} (linear or nonlinear regime), as well as on the way the nonlinear effects are treated at the receiver.

Notice that the optimal solution is seldom full power $P_{b,m}=P_{\max}$ since the interference-temperature constraints as well as the linear and nonlinear interference (of the satellite on itself) usually prevent this solution. 

As we will see later, the involved functions in Problem \ref{pb:main} depend on the channel gains $G_m^{(b',b)}$ and $F_{b,m}^{(\ell)}$.
From the satellite communication system side, we assume that all channel gains are known.
The channel gains for links from the secondary terrestrial users to the satellite are easily available, since they depend on the user location which can be obtained through GPS and the position of the satellite which is known in advance \cite{SATCOM:bousquet2011, SATCOM:caini1992}.
The gains for the links between the satellite terminals and the terrestrial devices can be listed into a database \cite{OPTI:lagunas2015}.
Nevertheless, these values may be affected by strong fading in adverse weather conditions \cite{SATCOM:bousquet2011}.
If these events are short in time, they can be overcome using conventional higher-layer protocols like re-transmission of missing data and buffering.
If the impairments last longer, this information can be updated to the network manager computing the power allocation when it is appropriate and so not so often.

\section{Conclusion}\label{sec:conclusion_chap1}

In this chapter, we have presented the model of the satellite communication system.
The transmission chain has been explained as well as the modeling of the \acrshort{HPA}, in order to obtain the output sample sequence according to the symbols at the input of the link.

The main optimization problem has been formulated, and its constraints related to the cognitive radio paradigm have been made explicit.
This problem constitutes the main thread of this thesis.

In Chapter~\ref{chap:l}, we give solutions to this problem under the assumption of a perfectly linear \acrshort{HPA}.
In Chapter~\ref{chap:capa} is devoted to the expression of the data rate in the presence of nonlinearity, then in Chapters~\ref{chap:nl} and \ref{chap:lnl} we focus on our optimization problem when the \acrshort{HPA} generates nonlinear effects.