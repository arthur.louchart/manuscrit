\chapter*{Conclusions and perspectives}
\addcontentsline{toc}{chapter}{Conclusions and perspectives}

The main objective of the thesis was to perform resource allocation to maximize the system sum-rate, while satisfying the interference temperature limits regarding the primary systems, by taking into account nonlinear impairments due to satellite components modeled by Volterra series.

In Chapter~\ref{chap:problem_statement}, we presented the satellite communication system.
We derived the signal model in presence of nonlinear effects resulting from \glsxtrfull{HPA} modeled as a memoryless polynomial, leading to Volterra series.
The paradigm of underlay cognitive radio, with the presence of primary network, was taken into account by relevant interference-temperature constraints that the cognitive satellite network has to not exceed.
Then the general optimization problem of sum-rate maximization has been stated.

In Chapter~\ref{chap:sp}, we introduced the concept of convex optimization and provided conventional procedure to solve non-convex problems.
We then presented non-convex problems with a particular form, namely \glsxtrfull{DC}, \glsxtrfull{LDC}, \glsxtrfull{GP}, \glsxtrfull{CGP} and \glsxtrfull{SP}, allowing to easily apply the \glsxtrfull{SCA} procedure.
We encountered these types of problems in the following chapters dealing with resource allocation.

In Chapter~\ref{chap:l}, we stated the sum-rate optimization problem in the case of a linear \acrshort{HPA} and proposed relevant resource allocations.
Even under this assumption, we encountered non-convex problems, for which we proposed algorithms for finding solutions.
Noting that sum-rate maximization was not fair among users, we then formulated and solved problems dealing with fairness between users.

In Chapter~\ref{chap:capa}, we given two closed-form expressions of the data rate in the presence of nonlinear effects, related to two different receivers.
The first one, called {\itshape nonlinearity-aware receiver}, exploiting the nonlinear effects as additional information.
The other one, called {\itshape nonlinearity-agnostic receiver}, considering the nonlinearities as additional noise.
We finally emphasized the mathematical properties of data rate expressions and involved terms.

In Chapter~\ref{chap:nl}, we stated the sum-rate optimization problem in the presence of nonlinear effects and using the nonlinearity-agnostic receiver.
Based on the corresponding data rate expression, the resulting problem boiled down to \acrshort{CGP} and we proposed an algorithm to find the relevant power allocation.
In addition, we also stated and solved related problems, namely the maximization of the minimum per-user data rate and the minimization of the sum-power subject to a target data rate.
These two problems boiled down to \acrshort{GP}.
Finally, we illustrated that the power allocations obtained under the assumption of a linear \acrshort{HPA} do not perform well in the presence of nonlinear interference.
The consideration of this interference in the optimization problem is fundamental.

In Chapter~\ref{chap:lnl}, we stated the sum-rate the optimization problem in the presence of nonlinear effects and using the nonlinearity-aware receiver.
From the expression of the data rate evaluated with this receiver, the resulting problem boiled down to \acrshort{SP} and we proposed an algorithm to find the relevant power allocation.
In addition, we also stated and solved related problems, namely the maximization of the minimum per-user data rate and the minimization of the sum-power subject to a target data rate.
These two problems also boiled down to \acrshort{SP}.
Finally, we illustrated that the power allocations obtained under the assumption of a linear \acrshort{HPA} do not perform well for the nonlinearity-aware receiver.
The consideration of the signomial expression of the data rate in the optimization problem is essential.

\section*{Perspectives}

The following concerns should be addressed in future works.

\subsection*{High-power amplifier model}
\begin{itemize}
	\item In Chapter~\ref{chap:problem_statement}, the Volterra series is truncated to the third order.
	It would be interesting to consider larger orders to see the impact on the terms involved in data rate expression.
	
	\item We used a memoryless polynomial model to characterize the input-output of the \acrshort{HPA}, leading to Volterra series.
	However, \acrshort{HPA}s have a memory effect.
	The use of \acrshort{HPA} model with memory \cite{HPA:morgan2006} would be interesting to investigate.
\end{itemize}

\subsection*{Data rate expression in presence of nonlinearities}
\begin{itemize}
	\item In Chapter~\ref{chap:capa}, we assume that the transmitted symbol sequence is complex-valued circularly-symmetric Gaussian random process.
	This assumption simplifies the decomposition of the fourth- and sixth-order moment.
	However, in the case of constellation currently used for satellite communications (e.g. APSK), the cumulant must be added in the moment decomposition.
	It would be interesting to observe the impact of this cumulant on the terms involved in the data rate expression.
	
	\item We assumed in Chapter~\ref{chap:capa} that the receiver decodes sample per sample, so the time and frequency correlations between the linear part and the nonlinear part are neglected. Actually, joint decoding by taking into account the whole process could improve the performance for each symbol detection.
\end{itemize}

\subsection*{Resource allocation}

\begin{itemize}
	\item In this thesis, we focused on power allocation.
	An extension has been proposed with the joint optimization of subband assignment and power allocation, under the assumption of a linear \acrshort{HPA}.
	It would be interesting to extend it to the nonlinear case, which would lead to {\itshape Mixed-Integer Signomial Programming} \cite{OPTI:lundell2012}.
	
	\item In \cite{OPTI:tohidi2021}, the authors perform resource allocation when the satellites belong to different operators under the assumption of a linear \acrshort{HPA}, leading to distributed optimization.
	It would be interesting to adapt their method in the presence of nonlinear effects.
\end{itemize}